package com.rnd.sample.unittest.calculator.service;

import org.springframework.stereotype.Service;

@Service
public class CalculatorService {

    /* PERKALIAN
     * diisi dengan 2 parameter
     * dapat int dan double
     */
    public int multiply(int a, int b) {
        return a * b;
    }

    /* PEMBAGIAN
     * diisi dengan 2 parameter
     * dapat int dan double
     */
    public int divide(int a, int b) {
        return a / b;
    }

    /* PENJUMLAHAN
     * diisi dengan 2 parameter
     * dapat int dan double
     */
    public int add(int a, int b) {
        return a + b;
    }

    /* PENGURANGAN
     * diisi dengan 2 parameter
     * dapat int dan double
     */
    public int substract(int a, int b) {
        return a - b;
    }


}
