package com.rnd.sample.unittest.transfer.exception;

public class TransferInvalidException extends Exception {

    public TransferInvalidException(String message) {
        super(message);
    }
}
