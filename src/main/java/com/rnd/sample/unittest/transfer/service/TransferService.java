package com.rnd.sample.unittest.transfer.service;

import com.rnd.sample.unittest.calculator.service.CalculatorService;
import com.rnd.sample.unittest.transfer.dto.TransferInput;
import com.rnd.sample.unittest.transfer.exception.TransferInvalidException;
import org.springframework.beans.factory.annotation.Autowired;

public class TransferService {

    /* Transfer
    trasnfer antar rekening
    method void
    akan call class calculator

    field
    - sourceAccountName
    - sourceAccountNumber
    - destinationAccountName
    - destinationAccountNumber

    */

    @Autowired
    private CalculatorService calculatorService;

    public void transferAntarRekening(TransferInput input) throws Exception {
    }

}
