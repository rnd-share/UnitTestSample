package com.rnd.sample.unittest.transfer.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransferInput {

    private String sourceAccountName;
    private String sourceAccountNumber;
    private String destinationAccountName;
    private String destinationAccountNumber;
    private double amount;

}
