package com.rnd.sample.unittest.login.controller;

import com.rnd.sample.unittest.login.dto.LoginRequest;
import com.rnd.sample.unittest.login.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/login")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping
    public ResponseEntity<Boolean> login(@RequestBody LoginRequest loginRequest) {
        return new ResponseEntity<>(
                loginService.login(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                ),
                HttpStatus.OK);
    }

}
