package com.rnd.sample.unittest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.rnd.sample.unittest"})
public class UnitTestSampleApplication{

    public static void main(String[] args) {
        SpringApplication.run(UnitTestSampleApplication.class);
    }

}
